# README #

The base file here is mdc77.less - compile this one.

### What is this repository for? ###

* Rapid development of PSO apps using LESS files to create CSS output
* Version 0.01
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* npm install -g less
* npm install -g less-plugin-clean-css

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Matt B
* Whoever wrote the initial mDesign LESS files